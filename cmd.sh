#!/bin/sh
docker build -t "stream-core-db" . || true
docker rm $(docker stop $(docker ps -a -q --filter name="stream-core-db" --format="{{.ID}}")) || true
docker run -i -d -p 7070:80 --name="stream-core-db" -v $PWD:/app "stream-core-db"
