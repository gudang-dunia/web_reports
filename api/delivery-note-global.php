<?php
include_once("../PHPJasperXML.inc.php");
include_once ('../setting.php');
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

$filename = isset($_GET['filename']) ? $_GET['filename'] : "delivery-note-global";
$company_id = isset($_GET['company_id']) ? ' AND ivt_delivery_note.company_id = "'.$_GET['company_id'].'"' : "";
$closing_period_id = isset($_GET['closing_period_id']) ? ' AND sls_sales_order.closing_by = "'.$_GET['closing_period_id'].'"' : "";
$closing_period = isset($_GET['closing_period']) ? $_GET['closing_period'] : " PERIODE ".date("d/m/Y",strtotime($_GET['from_date']))." to ".date("d/m/Y",strtotime($_GET['to_date']));
$transaction_status = isset($_GET['transaction_status']) ? ' AND sls_sales_order.confirm_status = "'.$_GET['transaction_status'].'"' : ' AND sls_sales_order.confirm_status != "Cancel"';
$payment_type = isset($_GET['payment_type']) ? ' AND mst_payment_type.name = "'.$_GET['payment_type'].'"' : '';
$delivery_id = isset($_GET['delivery_id']) ? ' AND ivt_delivery_note.id = "'.$_GET['delivery_id'].'"' : '';
$from_date = isset($_GET['from_date']) ? ' AND ivt_delivery_note.delivery_date >= "'.$_GET['from_date'].'"' : "";
$to_date = isset($_GET['to_date']) ? ' AND ivt_delivery_note.delivery_date <= "'.$_GET['to_date'].'"' : "";
if($closing_period_id != ""){
  $from_date = $closing_period_id;
  $to_date = "";
}
if($delivery_id != ""){
  $from_date = "";
  $to_date = "";
}

$query = '
          SELECT
            DATE( ivt_delivery_note.delivery_date ) as transaction_date,
            mst_company.id AS company_id,
            mst_company.NAME AS company_name,
            mst_courier.id AS courier_id,
            mst_courier.NAME AS courier_name,
            IFNULL( COUNT( sls_sales_order.id ), 0 ) AS packet,
            IFNULL( SUM( sls_sales_order_detail.quantity ), 0 ) AS quantity,
            IFNULL( SUM( sls_sales_order_detail.price ), 0 ) AS price,
            IFNULL( SUM( sls_sales_order_detail.voucer ), 0 ) AS voucer,
            IFNULL( SUM( sls_sales_order.courier_cost ), 0 ) AS courier_cost,
            IFNULL( SUM( sls_sales_order.cost_handler ), 0 ) AS cost_handler,
            IFNULL( SUM( sls_sales_order.transaction ), 0 ) AS total_transaction,
            SUM(IF((IFNULL(sls_sales_order_detail.status,0) = 1 OR sls_sales_order_detail.status <> "") ,mst_company.commission_price,IFNULL(mst_commission_rate.commission_price,mst_company.commission_price))*sls_sales_order_detail.quantity) as sum_commission
          FROM
            ivt_delivery_note
            INNER JOIN ivt_delivery_note_detail ON ivt_delivery_note_detail.delivery_note_id = ivt_delivery_note.id
            INNER JOIN sls_sales_order ON sls_sales_order.id = ivt_delivery_note_detail.sales_order_id
            INNER JOIN sls_sales_order_detail ON sls_sales_order_detail.sales_order_id = sls_sales_order.id
            LEFT JOIN mst_commission_rate ON mst_commission_rate.sales_id = sls_sales_order.sales_id
            AND sls_sales_order.transaction_date >= mst_commission_rate.start_date
            AND sls_sales_order.transaction_date <= mst_commission_rate.end_date
            INNER JOIN mst_company ON mst_company.id = ivt_delivery_note.company_id
            LEFT JOIN mst_courier ON mst_courier.id = ivt_delivery_note.courier_id
          WHERE 1 = 1
                '
                .$company_id
                .$from_date
                .$to_date
                .$delivery_id
                .'
          GROUP BY
            ivt_delivery_note.company_id,
            ivt_delivery_note.courier_id,
            DATE(ivt_delivery_note.delivery_date)
          ORDER BY
            ivt_delivery_note.company_id ASC,
            ivt_delivery_note.courier_id ASC,
            DATE(ivt_delivery_note.delivery_date) ASC
        ';
// var_dump($query);die;
$result = mysqli_query($conn, $query);
$data = mysqli_fetch_all ($result, MYSQLI_ASSOC);

$PHPJasperXML = new PHPJasperXML();
// $PHPJasperXML = new PHPJasperXML("en","XLS");
$PHPJasperXML->arrayParameter=array(
   "url_logo"=>"http://".$host."/gudangdunia_l.png"
  ,"title"=>"REPORT DELIVERY NOTE GLOBAL"
  ,"sub_title"=>strtoupper($closing_period)
  ,"periode_start"=>date("d/m/yy H:i",strtotime($_GET['from_date']))
  ,"periode_end"=>date("d/m/yy H:i",strtotime($_GET['to_date']))
);

$PHPJasperXML->load_xml_file("delivery-note-global.jrxml");
$PHPJasperXML->arraysqltable=$data;
$PHPJasperXML->outpage("I","reports-delivery-note-global.pdf"); //I||D||F
?>
