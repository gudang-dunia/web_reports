<?php
include_once("../PHPJasperXML.inc.php");
include_once ('../setting.php');
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

$filename = isset($_GET['filename']) ? $_GET['filename'] : "packing-commissions-global";
$title = (isset($_GET['payment_type'])) ? strtoupper($_GET['payment_type']):"GLOBAL";
$company_id = isset($_GET['company_id']) ? ' AND mst_commission_rate.company_id = "'.$_GET['company_id'].'"' : "";
$from_date = isset($_GET['from_date']) ? ' AND mst_commission_rate.start_date >= "'.$_GET['from_date'].'"' : "";
$to_date = isset($_GET['to_date']) ? ' AND mst_commission_rate.end_date <= "'.$_GET['to_date'].'"' : "";

$query = '
          SELECT
              mst_commission_rate.id,
              mst_commission_rate.commission_price,
              mst_company.id AS company_id,
              mst_company.NAME AS company_name,
              DATE_FORMAT( mst_commission_rate.start_date, "%d/%m/%Y %H:%i" ) AS start_date,
              DATE_FORMAT( mst_commission_rate.end_date, "%d/%m/%Y %H:%i" ) AS end_date,
              COUNT( sls_sales_order.id ) AS sum_paket,
              SUM(IFNULL( sls_sales_order_detail.quantity, 0 )) AS sum_botol,
              users.NAME AS author
          FROM
              mst_commission_rate
              LEFT JOIN users ON users.id = mst_commission_rate.sales_id
              LEFT JOIN mst_company ON mst_company.id = mst_commission_rate.company_id
              LEFT JOIN sls_sales_order ON (sls_sales_order.transaction_date >= mst_commission_rate.start_date AND sls_sales_order.transaction_date <= mst_commission_rate.end_date and sls_sales_order.sales_id = mst_commission_rate.sales_id)
              LEFT JOIN sls_sales_order_detail ON sls_sales_order_detail.sales_order_id = sls_sales_order.id
          WHERE 1 = 1
              '
              .$company_id
              .$from_date
              .$to_date
                .'
            GROUP BY
                mst_commission_rate.id
            ORDER BY
                mst_commission_rate.company_id ASC,
                users.name ASC,
                mst_commission_rate.start_date ASC
        ';
// echo $query;die;
$result = mysqli_query($conn, $query);
$data = mysqli_fetch_all ($result, MYSQLI_ASSOC);

if(isset($_GET['xls'])){
  $PHPJasperXML = new PHPJasperXML("en","XLSX");
  $PHPJasperXML->arrayParameter=array(
     "url_logo"=>"http://".$host."/gudangdunia_l.png"
    ,"title"=>"REPORT SALES COMISSION ".$title
    ,"sub_title"=> strtoupper(date("d/m/yy H:i",strtotime($_GET['from_date'])))." to ".strtoupper(date("d/m/yy H:i",strtotime($_GET['to_date'])))
    ,"periode_start"=>date("d/m/yy H:i",strtotime($_GET['from_date']))
    ,"periode_end"=>date("d/m/yy H:i",strtotime($_GET['to_date']))
  );

  $PHPJasperXML->load_xml_file("commissions-rate-detail.jrxml");
  $PHPJasperXML->arraysqltable=$data;
  $PHPJasperXML->outpage("I","reports-commissions-rate-detail.xls");
}else{
  $PHPJasperXML = new PHPJasperXML();
  $PHPJasperXML->arrayParameter=array(
     "url_logo"=>"http://".$host."/gudangdunia_l.png"
    ,"title"=>"REPORT SALES COMISSION ".$title
    ,"sub_title"=> strtoupper(date("d/m/yy H:i",strtotime($_GET['from_date'])))." to ".strtoupper(date("d/m/yy H:i",strtotime($_GET['to_date'])))
    ,"periode_start"=>date("d/m/yy",strtotime($_GET['from_date']))
    ,"periode_end"=>date("d/m/yy",strtotime($_GET['to_date']))
  );

  $PHPJasperXML->load_xml_file("commissions-rate-detail.jrxml");
  $PHPJasperXML->arraysqltable=$data;
  $PHPJasperXML->outpage("I","reports-commissions-rate-detail.pdf"); //I||D||F
}
?>
