<?php
include_once("../PHPJasperXML.inc.php");
include_once ('../setting.php');
error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE);

$query = '
SELECT
    sls_sales_order.*,
    DATE_FORMAT(sls_sales_order.transaction_date , "%d/%m/%Y %H:%i") AS transaction_date,
    CONCAT(IFNULL( mst_gender.NAME,"")," ",mst_customer.full_name) AS full_name,
    mst_customer.phone AS phone,
    mst_gender.id AS gender_id,
    mst_advertise.name AS advertise_name,
    mst_company.name AS company_name,
    IFNULL( mst_gender.NAME, "" ) AS gender_name,
    IFNULL( mst_bank.NAME, "" ) AS bank_name,
    IFNULL( mst_market.NAME, "" ) AS market_name,
    IFNULL( mst_courier.NAME, "" ) AS courier_name,
    IFNULL( mst_payment_type.NAME, "" ) AS payment_type_name,
    IFNULL( SUM( sls_sales_order_detail.quantity ), 0 ) AS quantity,
    IFNULL( SUM( sls_sales_order_detail.price ), 0 ) AS price,
    IFNULL( SUM( sls_sales_order_detail.voucer ), 0 ) AS voucer,
    IFNULL( SUM( sls_sales_order_detail.total_transaction ), 0 ) AS total_transaction,
    SUM(IFNULL(mst_commission_rate.commission_price,sls_sales_order_detail.commission_price)*sls_sales_order_detail.quantity) AS sum_commission,
    IFNULL( mst_customer_address.address, "" ) AS address,
    IFNULL( mst_customer_address.address_no, "" ) AS address_no,
    IFNULL( mst_customer_address.rt, "" ) AS rt,
    IFNULL( mst_customer_address.rw, "" ) AS rw,
    IFNULL( mst_customer_address.village, "" ) AS village,
    IFNULL( mst_customer_address.sub_district, "" ) AS sub_district,
    IFNULL( mst_customer_address.benchmark, "" ) AS benchmark,
    IFNULL( mst_customer_address.city_id, "" ) AS city_id,
    IFNULL( mst_customer_address.district, "" ) AS district,
    IFNULL( mst_customer_address.province_id, "" ) AS province_id,
    IFNULL( mst_customer_address.postal_code, "" ) AS postal_code
FROM
    sls_sales_order
    LEFT JOIN sls_sales_order_detail ON sls_sales_order_detail.sales_order_id = sls_sales_order.id
    LEFT JOIN mst_commission_rate ON mst_commission_rate.sales_id = sls_sales_order.sales_id
        AND sls_sales_order.transaction_date >= mst_commission_rate.start_date
        AND sls_sales_order.transaction_date <= mst_commission_rate.end_date
    LEFT JOIN mst_bank ON mst_bank.id = sls_sales_order.bank_id
    LEFT JOIN mst_market ON mst_market.id = sls_sales_order.market_id
    LEFT JOIN mst_courier ON mst_courier.id = sls_sales_order.courier_id
    LEFT JOIN mst_payment_type ON mst_payment_type.id = sls_sales_order.payment_type_id
    LEFT JOIN mst_customer ON mst_customer.id = sls_sales_order.customer_id
    LEFT JOIN mst_company ON mst_company.id = sls_sales_order.company_id
    LEFT JOIN mst_customer_address ON mst_customer_address.id = sls_sales_order.customer_address_id
    LEFT JOIN mst_advertise ON mst_advertise.id = sls_sales_order.advertise_id
    LEFT JOIN mst_gender ON mst_gender.id = mst_customer.gender_id
WHERE
    sls_sales_order.company_id = "DGT"
GROUP BY
    sls_sales_order.id
ORDER BY
    sls_sales_order.company_id ASC,
    sls_sales_order.author ASC,
    sls_sales_order.confirm_status ASC,
    sls_sales_order.transaction_date DESC
        ';
$result = mysqli_query($conn, $query);
$data = mysqli_fetch_all ($result, MYSQLI_ASSOC);

$PHPJasperXML = new PHPJasperXML();
// $PHPJasperXML = new PHPJasperXML("en","XLS");
$PHPJasperXML->arrayParameter=array(
   "url_logo"=>"http://".$host."/gudangdunia_l.png"
  ,"title"=>"REPORT COMMISSIONS DETAIL"
  ,"sub_title"=>"sub report"
  ,"periode_start"=>"12/12/2021"
  ,"periode_end"=>"12/12/2021"
);

$PHPJasperXML->load_xml_file("sales-order-commissions-detail.jrxml");
// $xml =  simplexml_load_file("sales-order-commissions.jrxml");
// $PHPJasperXML->xml_dismantle($xml);
$PHPJasperXML->arraysqltable=$data;
// $config["reportElement"] = ["backcolor"=>array("r"=>228,"g"=>211,"b"=>128)];
// $PHPJasperXML->element_staticText($config);
$PHPJasperXML->outpage("I","reports-sales-commissions-detail.pdf"); //I||D||F
?>
