<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'abd7bd83b0c5156621b5d862bc55a42b37296b8e',
        'name' => 'phpjasper/examples',
        'dev' => true,
    ),
    'versions' => array(
        'geekcom/phpjasper' => array(
            'pretty_version' => '3.3.1',
            'version' => '3.3.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../geekcom/phpjasper',
            'aliases' => array(),
            'reference' => 'df25051c1c50410d89d51fa84b8b6457b3c1e851',
            'dev_requirement' => false,
        ),
        'phpjasper/examples' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'abd7bd83b0c5156621b5d862bc55a42b37296b8e',
            'dev_requirement' => false,
        ),
    ),
);
